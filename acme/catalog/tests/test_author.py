from django.test import TestCase
from django.urls import reverse

from catalog.models import Author

# always use descriptive names for your tests
class AuthorListViewTest(TestCase):
    @classmethod   # this is a python decorator, not Django syntax. It simplifies creating a class method
    def setUpTestData(cls):
        # Create 3 authors for pagination tests
        number_of_authors = 3

        for author_id in range(number_of_authors):
            Author.objects.create(
                first_name=f'Christian {author_id}',
                last_name=f'Surname {author_id}',
            )

    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/catalog/authors/')    # the dummy "client" acts like a browser
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('authors'))     # the dummy "client" acts like a browser
        self.assertEqual(response.status_code, 200)

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('authors'))     # the dummy "client" acts like a browser
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/author_list.html')

    # update this to match the number of authors per pagination (see catalog/views.py)
     def test_pagination_is_two(self):
        response = self.client.get(reverse('authors'))     # the dummy "client" acts like a browser
        self.assertEqual(response.status_code, 200)
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'] == True)
        self.assertEqual(len(response.context['author_list']), 2)   # assumes 2 authors per page (this must match what is in catalog/views.py)

    def test_lists_all_authors(self):
        # Get second page and confirm it has (exactly) 1 remaining items
        response = self.client.get(reverse('authors')+'?page=2')     # the dummy "client" acts like a browser
        self.assertEqual(response.status_code, 200)
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'] == True)
        self.assertEqual(len(response.context['author_list']), 1)    # assert 1 authors on the final page (i.e. 3 authors, 2 per pagination)

