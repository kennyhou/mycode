from django.shortcuts import render
from django.http import HttpResponse
from datetime import date
import calendar
from calendar import HTMLCalendar


def index(request, year=date.today().year, month=date.today().month):
    year = int(year)
    month = int(month)
    if year < 1900 or year > 2099: year = date.today().year
    month_name = calendar.month_name[month]
    title = "Event Calendar - %s %s" % (month_name, year)
    cal = HTMLCalendar().formatmonth(year, month)
    ## This is the old line:
    ## return render(request, 'base.html', {'title': title, 'cal': cal})
    ## This is the new line (note the namespacing!)
    return render(request, 'events/calendar_base.html', {'title': title, 'cal': cal})

