#!/usr/bin/python3

from django.contrib import admin
from django.urls import path
from .import views

urlpatterns = [
    path('say-hello/', views.hello_world),
    path('custom/', views.customhome),
    path('admin/', admin.site.urls),
    path('', views.home),

]

