#!/usr/bin/python3

# imports from Django
from django.urls import path
from . import views

urlpatterns = [
    path('breakdown/', views.request_exam),
    path('request/',views.template_resp),
]

