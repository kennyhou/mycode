#!/usr/bin/python3
"""Alta3 Research | Testing String methods
   A simple script to test string methods. Introducing
   the unittest"""

# standard library
import unittest

# this class can be named anything
class TestStringMethods(unittest.TestCase):
    """Creating three string tests"""

    # testing str.capitalize()
    def test_title(self):
        self.assertEqual('wolverine'.capitalize(), 'Wolverine')

    # testing str.isupper()
    def test_isupper(self):
        self.assertTrue('STORM'.isupper())  # expects a true
        self.assertFalse('stOrM'.isupper()) # expects a false

    # testing str.split()
    def test_split(self):
        s = 'Nintendo Entertainment System'
        self.assertEqual(s.split(), ['Nintendo', 'Entertainment', 'System'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)

if __name__ == '__main__':
    unittest.main()

