from django.contrib import admin
from .models import Artist, Song   # these are the names of the models

admin.site.register(Artist)        # make it appear on the /admin/ site
admin.site.register(Song)          # make it appear on the /admin/ site

