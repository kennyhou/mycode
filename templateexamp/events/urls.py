from django.urls import path
from . import views  # import all views in the current package

# '' matches an empty string.
# It will also match the "/" as Django automatically removes the slash.
# Example: this matches both http://example.com and http://example.com/

urlpatterns = [
    path('', views.index, name='index'),
]

# name='index' is optional, but you should always name your URLs.
# We name URLs so they can be referred to in code (reverse lookup).
# URL reversing is common in both templates and views, so you will see several examples

