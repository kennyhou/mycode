#!/usr/bin/python3
"""Alta3 Research | rzfeeser@alta3.com
   A review on marshaling JSON. Marshal is another word for
   creating JSON."""

# JSON is part of the Python Standard Library
import json

def main():
    """runtime code"""
    ## create a blob of data to work with
    marvel_characters = [{"name": "Cyclops", "power": "energy weapon (eyes)"},
      {"name": "storm", "power": "control weather"}]

    ## display our Python data (a list containing two dictionaries)
    print(marvel_characters)

    ## open a new file in write mode
    with open("marvelguide.json", "w") as zfile:
        ## use the JSON library
        ## USAGE: json.dump(input data, file like object) ##
        json.dump(marvel_characters, zfile)

if __name__ == "__main__":
    main()

