from django.shortcuts import render
from .models import Artist  # Access data from our model

def artist(request):
    artist_list = Artist.objects.all()         # select everything from the database
   
    context = { 'artist_list':artist_list }    # create a dictionary of results
   
    return render(request, 'artist.html', context)   # return finished template
                                                     # named 'artist.html' as an
                                                     # HttpResponse object
def artist_details(request):
    artist_list = Artist.objects.all()         # select everything from the database

    context = { 'artist_list':artist_list }    # create a dictionary of results

    return render(request, 'artist_details.html', context)   # return finished template
                                                     # named 'artist.html' as an
                                                     # HttpResponse object
