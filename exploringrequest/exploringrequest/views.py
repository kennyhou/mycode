#!/usr/bin/python3

from django.http import JsonResponse 

# HTTPRequest object descriptions taken from
# https://docs.djangoproject.com/en/4.0/ref/request-response/

# Examining HttpRequest Objects - i.e. 'request'                                                                                      
def request_exam(request):                                                                                                            
    if request.method != 'GET':                                                                                                       
        return HttpResponse("Sorry, we only support the GET method at this time.")                                                    
                                                                                                                                      
    response_data = {}   # this is what we are going to send back                                                                     
    response_data['uri_path'] = request.path   # a string representing the full path to the requested page not including the scheme   
    response_data['uri_scheme'] = request.scheme                                                                                      

    # returns ALL of the headers
    response_data['request_headers'] = dict(request.headers)                                                                          
                                                                                                                                           
    return JsonResponse(response_data)  # abstraction to return json
    
# Return a template
def template_resp(request):

    response_data = {}
    response_data['method'] = request.method
    response_data['content_type'] = request.content_type
    response_data['GET_properties'] = request.GET.copy()  # .copy() returns a deep copy of a dict-like
                                                          # django object and returns a mutable one
    

    #request used to gen response, template, data to use to render template
    from django.shortcuts import render
    return render(request, 'mytemplate.html', response_data)

