from django.contrib import admin
from django.urls import include, path    # UPDATE - this line needs updated

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('events.urls')),    # the order here is important
]                                        # this says first try to match on `admin/`
                                         # if no match then go look in `events/urls.py` for matches
                                         # 

