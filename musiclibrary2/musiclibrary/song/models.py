from django.db import models   # base class for all models

class Artist(models.Model):
    name = models.CharField(max_length = 250)
    country = models.CharField(max_length = 150)
    birth_year = models.IntegerField()
    genre = models.CharField(max_length = 150)

    
class Song(models.Model):
    Title = models.CharField(max_length = 250)   # title of the song we are tracking 
    release_date = models.IntegerField()
    length = models.DateField()
    artist = models.ForeignKey('Artist', on_delete=models.CASCADE) # Sometime we have 2 or several models
                                                    # with fields associated with each other. One way
                                                    # to associate models with each other is by using 
                                                    # a "foreign key". This associates a record in a model 
                                                    # to another model (in our case Song and Arist model)

