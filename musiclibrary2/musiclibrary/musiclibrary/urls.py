#!/usr/bin/python3
  
from django.contrib import admin
from django.urls import path

from . import views            # ./views.py
from song.views import artist  # song/views.py - artist()
from song.views import artist_details  # song/views.py - artist()

urlpatterns = [
    path('say-hello/', views.hello_world),   # ./views.py - hello_world()
    path('admin/', admin.site.urls),

]


urlpatterns += [
    path('', views.home),      # ./views.py - home()
]

## NEW SECTION ##
urlpatterns += [
        path('artist/', artist),   # song/views.py - artist()
        path('artist_details/', artist_details),   # song/views.py - artist()
]
